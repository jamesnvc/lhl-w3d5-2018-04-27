//
//  WeirdStuffAnnotation.h
//  LocationsDemo
//
//  Created by James Cash on 27-04-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
@import MapKit;

@interface WeirdStuffAnnotation : NSObject <MKAnnotation>

@property (nonatomic,assign) CLLocationCoordinate2D coordinate;

@property (nonatomic,copy,nullable) NSString *title;

@property (nonatomic,copy,nullable) NSString *subtitle;

@end
