//
//  ViewController.m
//  LocationsDemo
//
//  Created by James Cash on 27-04-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
@import CoreLocation;
@import MapKit;
#import "WeirdStuffAnnotation.h"

@interface ViewController () <CLLocationManagerDelegate,MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *mapView;

@property (nonatomic,strong) CLLocationManager *locMgr;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.locMgr = [[CLLocationManager alloc] init];
    self.locMgr.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    self.locMgr.distanceFilter = 20;
    self.locMgr.delegate = self;

    [self.locMgr requestWhenInUseAuthorization];

    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    self.mapView.showsPointsOfInterest = YES;
    self.mapView.mapType = MKMapTypeStandard; //MKMapTypeHybridFlyover;

    [self.mapView registerClass:[MKMarkerAnnotationView class] forAnnotationViewWithReuseIdentifier:@"thingAnn"];

    WeirdStuffAnnotation *ann = [[WeirdStuffAnnotation alloc] init];
    ann.title = @"Something weird here";
    ann.subtitle = @"I dunno what";
    ann.coordinate = CLLocationCoordinate2DMake(40.6892656, -74.0466891);

    [self.mapView addAnnotation:ann];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    NSLog(@"Authorization status: %d", status);
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [manager startUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"Manager failed: %@", error.localizedDescription);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    NSLog(@"Updated locations %@", locations);
    CLLocation *loc = locations[0];
    [self.mapView
     setRegion:MKCoordinateRegionMake(loc.coordinate,
                                      MKCoordinateSpanMake(0.2, 0.2))
     animated:YES];
}

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[WeirdStuffAnnotation class]]) {
        MKMarkerAnnotationView *mark = (MKMarkerAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"thingAnn"                                                                              forAnnotation:annotation];

        mark.markerTintColor = [UIColor purpleColor];
        mark.glyphText = annotation.title;
        mark.titleVisibility = MKFeatureVisibilityVisible;
        mark.animatesWhenAdded = YES;

        return mark;
    }

    return nil;
}

@end
